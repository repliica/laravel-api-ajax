<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('produk', 'ProdukController@index');
Route::post('addproduk', 'ProdukController@store');
Route::post('deleteproduk/{id}', 'ProdukController@destroy');
Route::post('editproduk/{id}', 'ProdukController@update');
