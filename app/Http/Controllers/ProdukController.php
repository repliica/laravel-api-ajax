<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use Auth;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //   $products = Produk::all();
        $products = Produk::paginate(2);
          return response()->json($products,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data_array = $request->all();

        $nama_produk = $request->input('nama_produk');
        $harga_produk = $request->input('harga_produk');
        $jenis_produk = $request->input('jenis_produk');

        $produk = Produk::create([
            "nama_produk" => $nama_produk,
            "harga_produk" => $harga_produk,
            "jenis_produk" => $jenis_produk
        ]);
        // return $produk;
        return response()->json([
            "status"    => true,
            "message"   => "success"            
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_produk = $request->input('nama_produk');
        $harga_produk = $request->input('harga_produk');
        $jenis_produk = $request->input('jenis_produk');

        $produk = Produk::find($id);
        $produk->nama_produk = $nama_produk;
        $produk->harga_produk = $harga_produk;
        $produk->jenis_produk = $jenis_produk;
        $produk->save();
        if($produk->save()){
            return $produk;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $produk->delete();
        // return response()->json($user);
        // 200->sukses get
        // 201->sukses store
        return response()->json([
            "status"    => true,
            "message"   => "success",
            "data"      => $produk            
        ]);
    }
}
