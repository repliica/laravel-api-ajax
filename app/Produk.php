<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = [
        'nama_produk', 'jenis_produk', 'harga_produk','link_gambar_produk',
    ];
    
    protected $table = 'Produk';
}
