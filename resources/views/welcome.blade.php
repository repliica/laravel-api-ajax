<!DOCTYPE html>
<html>
<head>
    <title>Lazada</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-info mb-2">
        <div class="container">
            <a href="" class="navbar-brand">
                Lazada Mall
            </a>
        </div>
    </nav>
    <div class="card">
        <div class="card-body">
            <h1>Daftar Produk</h1>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary" id="show-button-add">Form Tambah Produk</button>
                </div>
                <br>
            </div>
            <div class="form-group" style="display:none">
                <form id="formadd">
                    <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="nama produk">
                    <br>
                    <input type="text" class="form-control" id="harga_produk" name="harga_produk" placeholder="harga produk">
                    <br>
                    <input type="text" class="form-control" id="jenis_produk" name="jenis_produk" placeholder="jenis produk">
                    <br>
                        <!-- <button class="btn btn-primary" id="button-add">Tambah Produk</button> -->
                    <button class="btn btn-success" type="button" id="button-add">Tambah Produk</button>
                </form>
            </div>
            <br><br>
            
            <div id="loader" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            <div id="datalistproduk" class="row"></div>
            <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body" id="modal-body">
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" id="buttonupdate">Save</button>
                    </div>
                  </div>  
                </div>
               </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            getproduk();
        });

        function getproduk(){
            $('#loader').show();
            $('#datalistproduk').html('');
            $.ajax({
                url: "http://localhost:8000/api/produk",
                type: "GET",
                dataType: "json",
                success: function(data){
                    $.each(data.data, function(index, element){
                        var data_temp = '<div class="card card-body mb-2 mr-2">'+
                                            '<p>Nama Produk : '+element.nama_produk+'</p>'+
                                            '<p>Harga : '+element.harga_produk+'</p>'+
                                            '<p>Kategori : '+element.jenis_produk+'</p>'+
                                            '<button class="btn btn-danger" id="deletebutton'+element.id+'">Delete</button>&nbsp;'+
                                            '<button class="btn btn-info" id="showupdatebutton'+element.id+'">Update</button>'+
                                        '</div>';
                        $('#datalistproduk').append(data_temp);
                        $('#deletebutton'+element.id).click(function(){
                            deleteproduk(element.id);
                        });

                        $('#showupdatebutton'+element.id).click(function(){
                            var formupdate_data =   '<div class="row">'+
                                                        '<input type="text" id="id_produk_update" hidden value="'+element.id+'">'+
                                                        '<label>Nama Produk</label><input class="form-control col-md-12" type="text" id="nama_produk_update" value="'+element.nama_produk+'">'+
                                                        '<label>Harga Produk</label><input class="form-control col-md-12" type="text" id="harga_produk_update" value="'+element.harga_produk+'">'+
                                                        '<label>Kategori Produk</label> <input class="form-control col-md-12" type="text" id="jenis_produk_update" value="'+element.jenis_produk+'">'+
                                                    '</div>';
                            $('#modal-body').html(formupdate_data); 
                            $('#myModal').modal('show');
                        });
                    });
                    $('#loader').hide();
                }
            });
        }   

        function deleteproduk(id){
            $.ajax({
                url: "http://localhost:8000/api/deleteproduk/"+id,
                type: "POST",
                data: {
                    
                },
                success: function(){
                    getproduk();
                }, error: function(){
                    alert('gagal delete');
                }
            });
        }

        $('#show-button-add').click(function(){
            $('.form-group').slideToggle('slow');
        });

        $('#button-add').click(function(){
            $.ajax({
                url: "http://localhost:8000/api/addproduk",
                type: "POST",
                data: {
                    nama_produk : $('#nama_produk').val(),
                    harga_produk: $('#harga_produk').val(),
                    jenis_produk: $('#jenis_produk').val()
                },
                success: function(){
                    getproduk();
                }, error: function(){
                    alert('gagal');
                }
            });
        });

        $('#buttonupdate').click(function(){
            var id = $('#id_produk_update').val();
            $.ajax({
                url: "http://localhost:8000/api/editproduk/"+id,
                type: "POST",
                data: {
                    nama_produk : $('#nama_produk_update').val(),
                    harga_produk: $('#harga_produk_update').val(),
                    jenis_produk: $('#jenis_produk_update').val()
                },
                success: function(){
                    $('#myModal').modal('hide');
                    getproduk();
                }, error: function(){
                    alert('gagal');
                }
            });
        });

    </script>
</body>
</html>